

document.addEventListener("DOMContentLoaded", function(){



    var connexion = new MovieDb();

    console.log(document.location);


    if(document.location.pathname.search("fiche-film.html") > 0){


        var params= (new URL (document.location)).searchParams;

        console.log(params.get("id"));

        if(params.get("id")!=null){

            connexion.requeteInfoFilm(params.get("id"));
            connexion.requeteInformationActeurs(params.get("id"));
            connexion.requeteTitrefilm(params.get("id"));



        }

        var btnTopPage = document.getElementById("myBtn");

        function topFunction() {
            window.scroll({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }

        btnTopPage.addEventListener('click', topFunction);

    }else{

        connexion.requeteDernierFilm();
        connexion.caroussel();
        connexion.requeteImageSwipper();
    }





});



class MovieDb {

    constructor() {

        this.APIkey = "17ba4542f1de5fc0dc40c8fdbc266bdc";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;

        this.totalActeurs = 6;

        this.totalSwipper=9;

    }


    requeteDernierFilm() {


        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequteDernierFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();


    }


    retourRequteDernierFilm(event) {


        var target = event.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            //console.log(data);

            this.afficheDernierFilm(data);

        }


    }


    afficheDernierFilm(data) {


        for (var i = 0; i < this.totalFilm; i++) {


            //clone de l'article
            var unArticle = document.querySelector(".template>.film").cloneNode(true);

            unArticle.querySelector("h3").innerText = data[i].title;

            if (data[i].overview === "") {
                unArticle.querySelector(".description").innerText = "sans description";
            } else {

                unArticle.querySelector(".description").innerText = data[i].overview;

            }


            var uneImage = unArticle.querySelector("img");

            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);


            var lienFilm = unArticle.querySelector("a");

            lienFilm.setAttribute("href", "fiche-film.html?id=" + data[i].id);


            document.querySelector("section.liste-films").appendChild(unArticle);
            unArticle.querySelector(".dateSortie").innerText="Date de sortie:"+data[i].release_date;
            unArticle.querySelector(".etoile").innerHTML = ("Nombres d'étoiles: " + data[i].vote_average + "/10 étoiles");

            console.log(data[i].title);

        }

    }


    requeteInfoFilm(idFilm) {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequteInfoFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + idFilm + "?language=" + this.lang + "&api_key=" + this.APIkey);
        xhr.send();


    }

    retourRequteInfoFilm(event) {


        var target = event.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log(data);

        }


    }


    requeteInformationActeurs(idFilm) {


        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequteInformationActeurs.bind(this));

        xhr.open("GET", this.baseURL + "movie/"+idFilm+"/credits"+"?api_key=" + this.APIkey);
        xhr.send();

    }

    retourRequteInformationActeurs(event) {


        var target = event.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).cast;

            this.afficheInformationsActeurs(data);

        }


    }


    afficheInformationsActeurs(data) {


        console.log(data);


        for (var i = 0; i < this.totalActeurs; i++) {

            var unActeur = document.querySelector(".template>article.acteur").cloneNode(true);


            unActeur.querySelector("h3").innerText = data[i].name;


            var uneImage = unActeur.querySelector("img");

            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurTeteAffiche[1] + data[i].profile_path);
            uneImage.setAttribute("alt", data[i].name);

            document.querySelector("div.liste-acteurs").appendChild(unActeur);

            console.log("allo");


        }

    }

    caroussel() {

        var mySwiper = new Swiper('.swiper-container', {
            Optional: 'parameters',
            direction: 'horizontal',
            loop: true,


            Navigation: 'arrows',
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        })


    }



    requeteTitrefilm(idFilm) {


        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteTitreFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/"+idFilm+"?language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();


    }


    retourRequeteTitreFilm(event) {


        var target = event.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            //console.log(data);

            this.afficheTitreFilm(data);

        }


    }


    afficheTitreFilm(data) {


        document.querySelector("h1").innerText = data.title;

        var Image=document.querySelector(".affiche");

        Image.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data.poster_path);


        document.querySelector(".vote").innerText="Note:"+data.vote_average;
        document.querySelector(".dateSortie").innerText="Date de sortie:"+data.release_date;
        document.querySelector(".langOrigine").innerText="Langue d'origine:"+data.original_language;
        document.querySelector(".dure").innerText="Durée:"+data.runtime+"minutes";
        document.querySelector(".budget").innerText="Budget:"+data.budget+"$";
        document.querySelector(".recette").innerText="Revenues:"+data.revenue+"$";

        console.log("allo");




        }


      requeteImageSwipper(){

          var xhr = new XMLHttpRequest();
          //xhr.withCredentials = true;

          xhr.addEventListener("readystatechange", this.retourRequeteImageSwipper.bind(this));

          xhr.open("GET", this.baseURL + "movie/now_playing"+"?language=" + this.lang + "&api_key=" + this.APIkey);

          xhr.send();



      }

    retourRequeteImageSwipper(event) {


        var target = event.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            //console.log(data);

            this.afficheImageSwipper(data);

        }

    }

    afficheImageSwipper(data) {


        for (var i = 0; i < this.totalSwipper; i++) {


            //clone de l'article
            var unArticle = document.querySelector("div.swiper-slide").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;


            var uneImage = unArticle.querySelector("img");

            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);

            var lienFilm = unArticle.querySelector("a");

            lienFilm.setAttribute("href", "fiche-film.html?id=" + data[i].id);


            document.querySelector("div.swiper-wrapper").appendChild(unArticle);

            console.log("allo");

        }


        }










}